console.log("Planner")

window.jQuery = $ 	= require('jquery');
window._			= require('underscore');
window.Popper 		= require('popper.js'),
window.Backbone		= require('backbone'),
window.Marionette	= require('backbone.marionette');

var Handlebars		= require("hbsfy/runtime"),
    bootstrap       = require('bootstrap');

Handlebars.registerHelper('eq', function(val, val2, block) {
	if(val == val2){
		return block.fn(this);
	}
});

Handlebars.registerHelper('not_eq', function(val, val2, block) {
	if(val != val2){
		return block.fn(this);
	}
});